app: app.o
	echo "Lint"
	cc app.o -o app

app.o: app.c
	echo "compile"
	cc -c app.c -o app.o

app.c:
	echo "prepare"
	echo "int main() { return 0; }" > app.c

clean:
	rm -f blah.o blah.c blah